var $ = require('jquery');
Backbone = require('backbone'),
_ = require('underscore');
Backbone.$ = $;

appRouter = require('./router');

$(document).ready(function() {
    Backbone.eventBus = _.extend({}, Backbone.Events);
    new appRouter();

    //Initialise history
    Backbone.history.start();
        
});
