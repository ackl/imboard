var Backbone = require('backbone');
var _ = require('underscore');
var $ = require('../jquery');
var noteTemplate = require('../templates/note.html');
Backbone.$ = $;

module.exports = Backbone.View.extend({
    initialize: function(opts) {
        this.cid = this.model.cid;
    },
    
    events: {
        'click .remove-note': 'removeNote'
    },

    removeNote: function() {
        gridster.remove_widget(this.$el);
        this.remove();
        this.model.destroy();
    }
});
