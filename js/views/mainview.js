var Backbone = require('backbone');
var _ = require('underscore');
var $ = require('../jquery');

var WallView = require('./wallview');
Backbone.$ = $;

module.exports = Backbone.View.extend({
    el: $('body'),
    initialize: function(opts) {

        this.wallViews = [];
        this.counter = opts.counter;
        this.wallViews[0] = new WallView({counter: this.counter});
        this.$el.append(this.wallViews[this.counter].$el);
        this.wallViews[0].initGridster();
        this.newWall();
        this.newWall();
        this.listenTo(Backbone.eventBus, 'disableSortable', this.disableSortable);
        this.listenTo(Backbone.eventBus, 'enableSortable', this.enableSortable);
    },

    events: {
        'click .add-wall-btn': 'newWall'
    },

    newWall: function() {
        this.counter += 1;
        this.wallViews.push(new WallView({counter: this.counter}));
        this.$el.append(this.wallViews[this.counter].$el);
        this.wallViews[this.counter].initGridster();
        var boardSelector = [];
        for (var i=0; i<=this.counter; i++) {
            boardSelector.push('#board-'+i+' ul');
        }
        boardSelector = boardSelector.join(', ');
        $(boardSelector).sortable({
                  cancel: '[contenteditable]',
                  placeholder: "ui-state-highlight",
                  //forcePlaceholderSize: true,
                  connectWith: ".board ul"
                }).disableSelection();
    },

    disableSortable: function() {
        $('.board ul').sortable('disable');
    },

    enableSortable: function() {
        $('.board ul').sortable('enable');
    }

});
