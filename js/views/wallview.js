var Backbone = require('backbone');
var noteTemplate = require('../templates/note.html');
var newNoteTemplate = require('../templates/newnote.html');
var wallTemplate = require('../templates/wall.html');
_ = require('underscore'),
$ = require('../jquery');
var Note = require('../models/note');
//var Notes = require('../collections/notes');
Backbone.$ = $;
var Firebase = require('firebase');
var Backbonefire = require('backbonefire');
var gridster = require('gridster');

var NoteView = require('./noteview');

module.exports = Backbone.View.extend({

    initialize: function(opts) {
        this.counter = opts.counter;

        var Notes = Backbone.Firebase.Collection.extend({
            model: Note,
            url: 'https://scorching-heat-9604.firebaseio.com/board' + this.counter
        });

        this.Notes = new Notes([]);

        this.$el.addClass('col-md-2 board board-' + this.counter);
        this.$el.attr('id', 'board-'+this.counter);

        this.listenTo(this.Notes, 'reset', this.addOldNotes);
        this.render();
    },

    events: {
        'click .add-note-btn': 'createNote'
    },

    render: function() {
        this.$el.html(wallTemplate);
        return this;
    },

    createNote: function() {
        var that = this;
        this.$('.gridster ul').append(newNoteTemplate({
            counter: that.counter
        }));
        Backbone.eventBus.trigger('disableSortable');
        //this.Notes.create({
            //content: $('.note-content-input').val(),
            //counter: that.counter
        //});
    },

    initGridster: function() {
        var that = this;
        this.listenTo(this.Notes, 'add', this.addNote);
    },

    addNote: function(model) {
        var that = this;
        this.$('.gridster ul').append(noteTemplate({
            content: model.get('content'),
            board: 0,
            id: model.cid,
            counter: that.counter
        }));

        var noteView = new NoteView({model: model, el: '.note-'+model.cid});
    }
});
