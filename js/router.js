var $ = require('jquery');
var Backbone = require('backbone');
_ = require('underscore');
Backbone.$ = $;

var WallView = require('./views/wallview');
var MainView = require('./views/mainview');
var Notes = require('./collections/notes');

/**
* Manages which todos view is rendered
*/
module.exports = Backbone.Router.extend({

    routes: {
        '': 'wall'
    },

    $page: $('body'),
    counter: 0,

    /**
     * upon starting up the app/router, make sure there is a main view
     */
    initialize: function() {
        this.notes = Notes;
        this.view = new MainView({counter: this.counter});
        this.$page.append(this.view.$el);
    },

    wall: function() {
    }

});
