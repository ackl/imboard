var $ = require('jquery');
var Backbone = require('backbone');
var _ = require('underscore');
var Note = require('../models/note');
var Firebase = require('firebase');
var Backbonefire = require('backbonefire');

Notes = Backbone.Firebase.Collection.extend({
	model: Note,
    url: 'https://scorching-heat-9604.firebaseio.com'

});
module.exports = new Notes([]);
