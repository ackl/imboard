var source = require('vinyl-source-stream'),
    streamify = require('gulp-streamify'),
    browserify = require('browserify'),
    uglify = require('gulp-uglify'),
    gulp = require('gulp'),
    less = require('gulp-less'),
    karma = require('karma').server,
    watchify = require('watchify'),
    csso = require('gulp-csso');

//default gulp task browserifies application js files in bundle and minifies js
gulp.task('default', function() {
  var bundleStream = browserify('./js/app.js').bundle()
  bundleStream
    .pipe(source('bundle.js'))
    .pipe(streamify(uglify()))
    .pipe(gulp.dest('./js/'))
});
 
gulp.task('less', function() {
    return gulp.src('less/global.less')
        .pipe(less())
        .pipe(csso())
        .pipe(gulp.dest('./css'));

    //gulp.src('less/*.less')
        //.pipe(less())
        //.pipe(gulp.dest('./css'));
});
//this task watches for changes application js files and compiles them
gulp.task('watch', function(){
	watch = true;
  browserifySetup('./js/app.js', 'bundle.js', './js/');
  gulp.watch('./less/*.less', ['less']); 
});


function browserifySetup(inputFile, outputFile, outputPath){
  //need to pass these three config options to browserify
  var b = browserify({
    cache: {},
    packageCache: {},
    fullPaths: true
  });
  //use watchify is watch flag set to true
  if (watch) {
	  //wrap watchify around browserify
	  b = watchify(b);

	  //compile the files when there is a change saved
	  b.on('update', function(){
	    buildFiles(b, outputFile, outputPath);
	  });
  }

  b.add(inputFile);
  buildFiles(b, outputFile, outputPath);
}

function buildFiles(b, outputFile, outputPath) {
  b.bundle()
    .on('error', function(err){
        console.log(err.message);
        this.end();
      })
    .pipe(source(outputFile))
    .pipe(gulp.dest(outputPath));
}
